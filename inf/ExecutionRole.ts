import * as aws from "@pulumi/aws";
import   {lamdaPolicy} from "./Policy"



export const executionRole = new aws.iam.Role("execution-role", {assumeRolePolicy: `{
 
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}`,
 name:'lamda-execution-role',
 managedPolicyArns: [ lamdaPolicy.arn]
}

);
